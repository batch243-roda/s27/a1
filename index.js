// Users
const users = `[
  {
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
  },
  {
    "id": 2,
    "firstName": "Jayson",
    "lastName": "Roda",
    "email": "jaysonroda@mail.com",
    "password": "roda123",
    "isAdmin": false,
    "mobileNo": "09237593671"
  }
]`;

// Orders
const orders = `[
  {
    "id": 1,
    "userId": 1,
    "productID" : 1,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 300
  },
  {
    "id": 2,
    "userId": 1,
    "productID" : 2,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 150
  },
  {
    "id": 3,
    "userId": 1,
    "productID" : 2,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 150
  },
  {
    "id": 4,
    "userId": 1,
    "productID" : 1,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 300
  },
  {
    "id": 5,
    "userId": 2,
    "productID" : 1,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 150
  },
  {
    "id": 6,
    "userId": 2,
    "productID" : 3,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
  }
]`;

// Products
const products = `[
  {
    "id": 1,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true
  },
  {
    "id": 2,
    "name": "Helmet",
    "description": "For safety in riding motorcycle.",
    "price": 150,
    "stocks": 1286,
    "isActive": true
  },
  {
    "id": 3,
    "name": "Computer",
    "description": "An object used by aliens. ^_^",
    "price": 1500,
    "stocks": 1286,
    "isActive": true
  }
]`;

console.log(JSON.parse(users));
console.log(JSON.parse(orders));
console.log(JSON.parse(products));
